const express = require("express");
const router = express.Router();
const { verify, decode } = require("../auth");

const userController = require("../controllers/userControllers");

//check if email exists
router.post("/email-exists", (req, res) => {

	userController.checkEmail(req.body)
    .then(result => res.send(result));
});

//register a user
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result));
});

//register a user
router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result));
});

router.get("/", (req, res) => {
	userController.getAllUsers()
    .then(result => res.send(result));
});

router.post("/details", verify, (req, res) => {
    let userData = decode(req.headers.authorization);

	userController.getProfile(userData.payload.email).then(result => res.send(result));
});

router.post("/enroll", verify, (req, res) => {
    let data = {
		userId: decode(req.headers.authorization),
		courseId: req.body.courseId
	};

	userController.enroll(data).then(result => res.send(result));
});

module.exports = router;
