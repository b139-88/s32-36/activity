
const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers");

const { verify } = require("../auth");

//create a course
router.post("/create-course", verify, (req, res) => {
	courseController.createCourse(req.body).then(result => res.send(result));
});


//retrieving all courses
router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
});


//retrieving only active courses
router.get("/active-courses", (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result));
});

//get a specific course using findOne()
router.get("/specific-course", (req, res) => {
	courseController.getSpecificCourse(req.body).then(result => res.send(result));
});

//get specific course using findById()
router.get("/:id", (req, res) => {
	courseController.getCourseById(req.params).then(result => res.send(result));
});

//update isActive status of the course using findOneAndUpdate()
router.put("/archive", (req, res) => {
	courseController.archiveCourse(req.body).then(result => res.send(result));
});

//update isActive status of the course using findByIdAndUpdate()
router.put("/unarchive", (req, res) => {
	courseController.unarchiveCourse(req.body).then(result => res.send(result));
});


//update isActive status of the course using findOneById()
router.put("/:id/archive", (req, res) => {
	courseController.archiveCourseById(req.params).then(result => res.send(result));
});

//update isActive status of the course using findOneById()
router.put("/:id/unarchive", (req, res) => {
	courseController.unarchiveCourseById(req.params).then(result => res.send(result));
});

//delete course using findOneAndDelete()
router.delete("/delete-course", verify, (req, res) => {
	courseController.deleteCourse(req.body).then(result => res.send(result));
});

//delete course using findByIdAndDelete()
router.delete("/:id/delete", (req, res) => {
	courseController.deleteCourseById(req.params).then(result => res.send(result));
});

//delete course using findByIdAndDelete()
router.delete("/:id/edit", verify, (req, res) => {
	courseController.deleteCourseById(req.params.id, req.body).then(result => res.send(result));
});

module.exports = router;
