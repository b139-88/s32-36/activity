const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();

const app = express();

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Routes
const courseRoutes = require("./routes/courseRoutes");
const userRoutes = require("./routes/userRoutes");

app.use("/api/courses", courseRoutes);
app.use("/api/users", userRoutes);

//mongodb connection
mongoose.connect(process.env.MONGODB_CS, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

app.listen(process.env.PORT || 8888, () => console.log(`Server running at port ${process.env.PORT}`))

