
const Course = require("../models/Course");

module.exports.createCourse = (reqBody) => {
	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	});
};

module.exports.getAllCourses = () => {

	return Course.find().then((result, error) => {
		if(error){
			return false;
		} else {
			return result;
		}
	});
};


module.exports.getActiveCourses = () => {
	return Course.find({
		isActive: true
	}).then((result, err) => {
		if(err) {
			return false;
		} else {
			return result;
		}
	});
};

module.exports.getSpecificCourse = (reqBody) => {
	return Course.findOne({
		courseName: reqBody.courseName
	}).then((result, error) => {
		if(error) {
			return `No course found with name ${reqBody.courseName} `;
		} else {
			return result;
		}
	});
};

module.exports.getCourseById = (params) => {
	return Course.findById(params.id).then((result, err) => {
		if(err) {
			return false;
		} else {
			return result;
		}
	});
};

module.exports.archiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: false
	};

	let options = {
		new: true
	};

	return Course.findOneAndUpdate({
		courseName: reqBody.courseName
	}, courseStatus, options).then((result, err) => {
		if(err) {
			return err;
		} else {
			if (result == null) {
				return `No course found with name ${reqBody.courseName} `;
			} else {
				return result;
			}
		}
	});
};

module.exports.unarchiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: true
	};

	let options = {
		new: true
	};

	return Course.findOneAndUpdate({
		courseName: reqBody.courseName
	}, courseStatus, options).then((result, err) => {
		if(err) {
			return err;
		} else {
			if (result == null) {
				return `No course found with name ${reqBody.courseName} `;
			} else {
				return result;
			}
		}
	});
};

module.exports.archiveCourseById = (params) => {
	let courseStatus = {
		isActive: false
	};

	let options = {
		new: true
	};

	return Course.findByIdAndUpdate(params.id, courseStatus, options).then((result, err) => {
		if(err) {
			return err;
		} else {
			if (result == null) {
				return `No course found.`;
			} else {
				return result;
			}
		}
	});
};

module.exports.unarchiveCourseById = (params) => {
	let courseStatus = {
		isActive: true
	};

	let options = {
		new: true
	};

	return Course.findByIdAndUpdate(params.id, courseStatus, options).then((result, err) => {
		if(err) {
			return err;
		} else {
			if (result == null) {
				return `No course found.`;
			} else {
				return result;
			}
		}
	});
};

module.exports.deleteCourse = (reqBody) => {
	return Course.findOneAndDelete({
		courseName: reqBody.courseName
	}).then((result, error) => {
		if(result == null) {
			return `No course found with name ${reqBody.courseName} `;
		} else {
			if (result) {
				return true;
			} else {
				return error;
			}
		}
	});
};

module.exports.deleteCourseById = (params) => {
	return Course.findByIdAndDelete(params.id)
	.then((result, error) => {
		if(result == null) {
			return `No course found.`;
		} else {
			if (result) {
				return true;
			} else {
				return error;
			}
		}
	});
};

module.exports.editCourse = (id, reqBody) => {
	let updateCourse = {
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(id, updateCourse, { new: true })
	.then((result, error) => {
		if(result == null) {
			return `No course found.`;
		} else {
			if (result) {
				return true;
			} else {
				return error;
			}
		}
	});
};