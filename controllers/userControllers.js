const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {
		// console.log(email)
		if(result != null){
			return `Email already exists`;
		} else {
			console.log(result);	//null bec email does not exist
			if(result == null){
				return true;
			} else {
				return error;
			}
		}
	});
};

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	});
	//save()
	return newUser.save().then( (result, error) => {
		if(result){
			return true;
		} else {
			return error;
		}
	});
};

module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result;
		} else {
			return error;
		}
	});
};

module.exports.login = (reqBody) => {
    return User.findOne({
        email: reqBody.email
    }).then((result, error) => {
        if (result == null) {
            return false;
        } else {
            let isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect){
                return {
                    access: auth.createAccessToken(result)
                };
            } else {
                return false;
            }
        }
    });
};

module.exports.getProfile = (email) => {
    return User.findOne({
        email: email
    }).then((result, error) => {
        if (result == null) {
            return false;
        } else {
            result.password = "";
            return result;
        }
    });
};

module.exports.enroll = async (data) => {
	const {userId, courseId} = data;

	await User.findById(userId).then((result, error) => {
		if (error) {
			return error;
		} else {
			result.enrollments.push({
				courseId: courseId
			});
		}
	});

	await Course.findById(courseId).then((result, error) => {
		if (error) {
			return error;
		} else {

		}
	});
};